MACSPOOFER
==========

Usage
-----

This library is intended to be used with LD_PRELOAD to make every network
interface appear to have a specific MAC address, set by the environment.

The MAC address of the loopback interface is never spoofed.

    $ LD_PRELOAD=/usr/local/lib/macspoofer.so ifconfig
    eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet 172.17.0.2  netmask 255.255.0.0  broadcast 172.17.255.255
            ether 02:42:ac:11:00:02  txqueuelen 0  (Ethernet)
            RX packets 3  bytes 450 (450.0 B)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 0  bytes 0 (0.0 B)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

    lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
            inet 127.0.0.1  netmask 255.0.0.0
            loop  txqueuelen 1000  (Local Loopback)
            RX packets 0  bytes 0 (0.0 B)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 0  bytes 0 (0.0 B)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

    $ export MACSPOOFER=42:42:42:42:42:42
    $ LD_PRELOAD=/usr/local/lib/macspoofer.so ifconfig
    eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet 172.17.0.2  netmask 255.255.0.0  broadcast 172.17.255.255
            ether 42:42:42:42:42:42  txqueuelen 0  (Ethernet)
            RX packets 4  bytes 560 (560.0 B)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 0  bytes 0 (0.0 B)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

    lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
            inet 127.0.0.1  netmask 255.0.0.0
            loop  txqueuelen 1000  (Local Loopback)
            RX packets 0  bytes 0 (0.0 B)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 0  bytes 0 (0.0 B)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

You may add the path to this library in /etc/ld.so/preload to make it work
globally without having to set the LD_PRELOAD environment variable.


Usage with Docker
-----------------

You may add the following instructions to your Dockerfile

    COPY --from=registry.cri.epita.fr/cri/docker/macspoofer /macspoofer/macspoofer.so /usr/local/lib/
    RUN echo "/usr/local/lib/macspoofer.so" >> /etc/ld.so.preload
    ENV MACSPOOFER 42:42:42:42:42:42


Technical details
-----------------

Spoofing is done by shadowing calls to the libc `ioctl` syscall wrapper, it
will not work with programs using a netlink socket to get network devices
information (such as `ip` from the `iproute2` package) or by parsing files in
`/sys/class/net/*`.
