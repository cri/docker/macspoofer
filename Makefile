CFLAGS = -fPIC -D_GNU_SOURCE -Werror -Wall -Wextra
LDFLAGS = -ldl

LIB_NAME = macspoofer.so
OBJS = macspoofer.o

all: $(LIB_NAME)

$(LIB_NAME): $(OBJS) Makefile
	$(CC) -shared $(CLFAGS) $(LDFLAGS) -o '$@' $(OBJS)

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) "$(LIB_NAME)"

.PHONY: all clean distclean
.INTERMEDIATE: $(OBJS)
