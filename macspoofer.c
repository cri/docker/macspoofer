#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <dlfcn.h>

#include <sys/ioctl.h>
#include <net/if.h>

#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/if_arp.h>

#define IOCTL_ARGS_SIZE (2 * sizeof (int) + sizeof (struct ifreq))
typedef int (*ioctl_decl_t)(int fd, int request, ...);

static ioctl_decl_t real_ioctl;
static struct sockaddr_ll spoofed_addr;

void spoof_hwaddr(struct sockaddr *sockaddr)
{
	if (spoofed_addr.sll_family != sockaddr->sa_family)
		return;

	memcpy(sockaddr->sa_data, spoofed_addr.sll_addr,
	       sizeof (spoofed_addr.sll_addr));
}

int ioctl(int fd, unsigned long request, ...)
{
	void *args = __builtin_apply_args();
	void *result = __builtin_apply((void *) real_ioctl, args,
	                               IOCTL_ARGS_SIZE);

	(void) fd;

	if (request == SIOCGIFHWADDR)
	{
		va_list vargs;
		va_start(vargs, request);

		struct ifreq *ifreq = va_arg(vargs, struct ifreq *);

		if (ifreq->ifr_hwaddr.sa_family == ARPHRD_ETHER)
			spoof_hwaddr(&ifreq->ifr_hwaddr);

		va_end(vargs);
	}

	__builtin_return(result);
}

__attribute__((constructor)) static void _init(void)
{
	const char *hwaddr;

	if ((hwaddr = getenv("MACSPOOFER")))
	{
		int r = sscanf(hwaddr, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%*c",
		               spoofed_addr.sll_addr,
		               spoofed_addr.sll_addr + 1,
		               spoofed_addr.sll_addr + 2,
		               spoofed_addr.sll_addr + 3,
		               spoofed_addr.sll_addr + 4,
		               spoofed_addr.sll_addr + 5);

		if (r == 6)
			spoofed_addr.sll_family = ARPHRD_ETHER;
	}

	if (!(real_ioctl = dlsym(RTLD_NEXT, "ioctl")))
	{
		fprintf(stderr, "%s\n", dlerror());
		exit(EXIT_FAILURE);
	}
}
